package com.jsite.modules.flowable.web;

import com.baidu.ueditor.ActionEnter;
import com.jsite.common.io.ResourceUtils;
import com.jsite.common.utils.FileUtils;
import com.jsite.common.utils.UploadUtils4;
import com.jsite.common.web.BaseController;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(value = "${adminPath}/ue/upload")
public class UeUploadController extends BaseController {


    @RequestMapping(value = "/enter", method = RequestMethod.GET)
    public void enterUE(HttpServletRequest request, HttpServletResponse response) {
        try {
            request.setCharacterEncoding("utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        response.setHeader("Content-Type", "text/html");
        String rootPath = request.getSession().getServletContext().getRealPath("/");
        try {
            Resource resource = ResourceUtils.getResource("classpath:static/formdesign/js/ueditor/jsp/");

            String toFilePath = rootPath + "a/ue/upload/";
            File dirFile = new File(toFilePath);
            File toFile = new File(toFilePath, "config.json");
            if (!toFile.exists()) {
                dirFile.mkdirs();

                InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("static/formdesign/js/ueditor/jsp/config.json");
                FileUtils.writeByteArrayToFile(toFile, IOUtils.toByteArray(inputStream));
            }

            String exec = new ActionEnter(request, rootPath).exec();
            PrintWriter writer = response.getWriter();
            writer.write(exec);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * ueditor上传图片的方法
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/uploadimage", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> uploadNewsImg(HttpServletRequest request, HttpServletResponse response) {
        UploadUtils4.UploadResult upResult = UploadUtils4.getInstance().uploadFile(request).get(0);

        Map<String, Object> result = new HashMap<>();
        result.put("state", "SUCCESS");
        result.put("url", upResult.fileRltvPath + upResult.fileName);
        result.put("original", "");
        result.put("type", upResult.fileName.substring(upResult.fileName.lastIndexOf(".")).toLowerCase());
        result.put("size", upResult.size);
        result.put("title", upResult.fileName);
        return result;
    }


    /**
     * ueditor文件上传方法
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/uploadfile", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> uploadFile(HttpServletRequest request, HttpServletResponse response) {
        UploadUtils4.UploadResult upResult = UploadUtils4.getInstance().uploadFile(request).get(0);

        Map<String, Object> result = new HashMap<>();
        result.put("state", "SUCCESS");
        result.put("url", upResult.fileRltvPath + upResult.fileName);
        result.put("original", "");
        result.put("type", upResult.fileName.substring(upResult.fileName.lastIndexOf(".")).toLowerCase());
        result.put("size", upResult.size);
        result.put("title", upResult.fileName);
        return result;
    }
}
