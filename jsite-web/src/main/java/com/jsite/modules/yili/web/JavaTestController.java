/**
 * Copyright &copy; 2017-2019 <a href="https://gitee.com/baseweb/JSite">JSite</a> All rights reserved.
 */
package com.jsite.modules.yili.web;

import com.jsite.common.config.Global;
import com.jsite.common.lang.DateUtils;
import com.jsite.common.lang.StringUtils;
import com.jsite.common.persistence.Page;
import com.jsite.common.web.BaseController;
import com.jsite.modules.yili.entity.JavaTest;
import com.jsite.modules.yili.service.JavaTestService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * JAVA机试表生成Controller
 * @author liuruijun
 * @version 2020-06-09
 */
@Controller
@RequestMapping(value = "${adminPath}/yili/javaTest")
public class JavaTestController extends BaseController {

	@Autowired
	private JavaTestService javaTestService;
	
	@ModelAttribute
	public JavaTest get(@RequestParam(required=false) String id) {
		JavaTest entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = javaTestService.get(id);
		}
		if (entity == null){
			entity = new JavaTest();
		}
		return entity;
	}
	
	@RequiresPermissions("yili:javaTest:view")
	@RequestMapping(value = {"list", ""})
	public String list() {
		return "modules/yili/javaTestList";
	}
	
	@RequiresPermissions("yili:javaTest:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<JavaTest> listData(JavaTest javaTest, HttpServletRequest request, HttpServletResponse response) {
		Page<JavaTest> page = javaTestService.findPage(new Page<>(request, response), javaTest);
		return page;
	}

	@RequiresPermissions("yili:javaTest:view")
	@RequestMapping(value = "form")
	public String form(JavaTest javaTest, Model model) {
		model.addAttribute("javaTest", javaTest);
		return "modules/yili/javaTestForm";
	}

	@RequiresPermissions("yili:javaTest:edit")
	@RequestMapping(value = "save")
	@ResponseBody
	public String save(JavaTest javaTest) {

		if (StringUtils.isBlank(javaTest.getId())) {
			JavaTest obj = javaTestService.getByName(javaTest.getName(), javaTest.getOffice().getId());
			if (obj != null) {
				return renderResult(Global.FALSE, "当前部门已存在该人员信息");
			}

			String code = DateUtils.getDate("yyyyMMdd:") + (javaTestService.getCount()+1);
			javaTest.setCode(code);
		}

		javaTestService.save(javaTest);
		return renderResult(Global.TRUE, "保存JAVA机试信息成功");
	}
	
	@RequiresPermissions("yili:javaTest:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(JavaTest javaTest) {
		javaTestService.delete(javaTest);
		return renderResult(Global.TRUE, "删除JAVA机试信息成功");
	}

}