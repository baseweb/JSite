/*
 * @author liuruijun
 * @version 2017-4-18
 */
$(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' /* optional */
    });
});

function ajaxSubmitForm(form, callback, dataType, async, message) {
    js.loading(message);
    $("#btnSubmit").attr("disabled", true);

    form.ajaxSubmit({
        type: "POST",
        url: form.attr("action"),
        dataType: dataType,
        async: async,
        error: function (data) {
            $("#btnSubmit").attr("disabled", false);
            js.showErrorMessage(data.responseText);
            js.closeLoading();
        },
        success: function (data) {
            $("#btnSubmit").attr("disabled", false);
            js.closeLoading();
            if (typeof callback == "function") {
                callback(data);
            } else {
                js.print(data);
            }
        }
    });
};

$(document).ready(function () {
    $("#loginForm").validate({
        submitHandler: function submitHandler(form) {
            ajaxSubmitForm($(form), function (data) {
                if (data.isValidateCodeLogin) {
                    $("#isValidCodeLogin").show();
                    $("#validCodeImg").click();
                }
                if (data.result == "false") {
                    js.showErrorMessage(data.message);
                } else {
                    js.loading();
                    location = ctx;
                }
            }, "json", true);
        },
        rules: {
            username: 'required',
            password: 'required',
        }
    });

    $("li").hover(function () {
        $("li").addClass('show');
        $("a[data-toggle='dropdown']").attr('aria-expanded', 'true');
        $("li div").addClass('show');
    }, function () {
        window.setTimeout(function () {
            $("li").removeClass('show');
            $("a[data-toggle='dropdown']").attr('aria-expanded', 'false');
            $("li div").removeClass('show');
        }, 200);

    });

});